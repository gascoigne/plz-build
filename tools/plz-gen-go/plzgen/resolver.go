package plzgen

import (
	"fmt"
	"strings"
)

type Resolver struct {
	LocalModule string
	// map of module import path -> module
	modulesByPath map[string]*Module
	// map of module dir -> module
	modulesByDir map[string]*Module
	// map of package import path -> module
	packageLookupCache map[string]*Module
}

func NewResolver(localModule string, modules map[string]*Module) *Resolver {
	modulesByDir := make(map[string]*Module)
	for _, m := range modules {
		if m.Dir == "" {
			panic(fmt.Sprintf("dir not present for module %v", m.Path))
		}
		modulesByDir[m.Dir] = m
		if m.Replace != nil {
			if m.Replace.Dir == "" {
				panic(fmt.Sprintf("dir not present for module %v", m.Replace.Path))
			}
			modulesByDir[m.Replace.Dir] = m.Replace
		}
	}
	return &Resolver{
		LocalModule:        localModule,
		modulesByPath:      modules,
		modulesByDir:       modulesByDir,
		packageLookupCache: make(map[string]*Module),
	}
}

func (r *Resolver) Resolve(importPath string) (string, error) {
	if r.PackageIsInLocalModule(importPath) {
		path := strings.TrimPrefix(importPath, r.LocalModule)
		path = strings.TrimLeft(path, "/")
		return "//" + path, nil
	}

	module, err := r.ResolveModule(importPath)
	if err != nil {
		return "", err
	}

	return "//third_party/go:" + sanitizeTargetName(module.Path), nil
}

func (r *Resolver) PackageIsInLocalModule(importPath string) bool {
	return strings.HasPrefix(importPath, r.LocalModule)
}

func (r *Resolver) ResolveModuleByDir(dir string) (*Module, error) {
	for prefix, module := range r.modulesByDir {
		if strings.HasPrefix(dir, prefix) {
			return module, nil
		}
	}

	return nil, fmt.Errorf("couldn't resolve dir %v to module", dir)
}

func (r *Resolver) ResolveModule(importPath string) (*Module, error) {
	if m, ok := r.packageLookupCache[importPath]; ok {
		return m, nil
	}

	longestPrefix := 0
	var moduleMatch *Module

	for prefix, module := range r.modulesByPath {
		if strings.HasPrefix(importPath, prefix) && len(prefix) > longestPrefix {
			longestPrefix = len(prefix)
			moduleMatch = module
		}
	}

	if moduleMatch == nil {
		return nil, fmt.Errorf("couldn't resolve import %v to module", importPath)
	}

	r.packageLookupCache[importPath] = moduleMatch

	return moduleMatch, nil
}

func sanitizeTargetName(name string) string {
	name = strings.ReplaceAll(name, "/", "_")
	name = strings.ReplaceAll(name, "-", "_")
	name = strings.ReplaceAll(name, ".", "_")
	return name
}
