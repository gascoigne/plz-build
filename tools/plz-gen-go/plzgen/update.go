package plzgen

import (
	"context"

	"github.com/sirupsen/logrus"
)

var Go = &Command{
	Command: "go",
}

func UpdateLocal() {
	ctx := context.Background()

	modFile, err := parseModFile()
	if err != nil {
		logrus.Fatalf("%v", err)
	}

	localModule := modFile.Module.Mod.Path

	pkgs, err := localPackages(ctx, localModule)
	if err != nil {
		logrus.Fatalf("%v", err)
	}

	// for _, pkg := range pkgs {
	// 	logrus.Infof("%#v", *pkg)
	// }

	buildlist, err := moduleBuildList(ctx)
	if err != nil {
		logrus.Fatalf("%v", err)
	}

	resolver := NewResolver(localModule, buildlist)

	err = buildModuleDeps(ctx, pkgs, resolver)
	if err != nil {
		logrus.Fatalf("%v", err)
	}

	// for _, mod := range buildlist {
	// 	logrus.Infof("%#v", *mod)
	// }

	err = writeLocalBuildFiles(pkgs, resolver)
	if err != nil {
		logrus.Fatalf("%v", err)
	}

	err = writeThirdParty(buildlist)
	if err != nil {
		logrus.Fatalf("%v", err)
	}
}
