package plzgen

import (
	"text/template"
)

var thirdPartyTmpl = template.Must(template.New("").Parse(`
subrepo = subrepo_name()
subrepo = f"///{subrepo}" if subrepo else ""

{{- range $k, $v := . }}
go_module(
    name = "{{$v.Name}}",
    install = [
{{- range $k, $v := $v.Install }}
        "{{ $k }}",
{{- end }}
    ],
{{- with $v.Replace }}
    replace = "{{ . }}",
{{- end }}
    module = "{{$v.Module}}",
    version = "{{$v.Version}}",
    visibility = ["//..."],
    deps = [
{{- range $k, $v := $v.Deps }}
        "{{ $k }}",
{{- end }}
    ],
)
{{ end }}`))

func writeThirdParty(modules map[string]*Module) error {
	type Data struct {
		Name    string
		Module  string
		Replace string
		Version string
		Deps    map[string]*struct{}
		Install map[string]*struct{}
	}

	data := make(map[string]*Data)
	for _, module := range modules {
		targetName := sanitizeTargetName(module.Path)
		if _, ok := data[targetName]; ok {
			continue
		}

		modulePath := module.Path
		replacePath := ""

		if module.Replace != nil {
			module = module.Replace
			replacePath = module.Path
		}

		if len(module.RequiredPackages) == 0 {
			continue
		}

		moduleData := &Data{
			Name:    targetName,
			Module:  modulePath,
			Version: module.Version,
			Deps:    map[string]*struct{}{},
			Install: module.RequiredPackages,
		}

		if replacePath != "" {
			moduleData.Replace = replacePath
		}

		for dep := range module.Deps {
			moduleData.Deps[":"+sanitizeTargetName(dep)] = nil
		}

		data[targetName] = moduleData
	}

	err := templateBuildFile("third_party/go/BUILD", thirdPartyTmpl, data)
	if err != nil {
		return err
	}

	return nil
}
