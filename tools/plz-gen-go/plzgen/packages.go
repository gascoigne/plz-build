package plzgen

import (
	"context"
	"fmt"
	"go/ast"
	"go/build"
	"go/parser"
	"go/token"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

type Package struct {
	Name            string
	Path            string
	SourceFiles     []string
	TestSourceFiles []string
	Imports         []string
	TestImports     []string
	IsMain          bool
	IsLocal         bool
}

func (g *Package) FullTargetName() string {
	return "//" + g.Path
}

func localPackagePaths(ctx context.Context) ([]string, error) {
	return Go.RunLines(ctx, "list", "./...")
}

func localPackages(ctx context.Context, selfModule string) ([]*Package, error) {
	paths, err := localPackagePaths(ctx)
	if err != nil {
		return nil, err
	}

	pkgs := make([]*Package, len(paths))
	for i, path := range paths {
		if !strings.HasPrefix(path, selfModule) {
			panic(fmt.Sprintf("local package path doesn't match module name: %v (module %v)", path, selfModule))
		}

		path = path[len(selfModule):]
		path = strings.TrimLeft(path, "/")

		if path == "" {
			path = "./"
		}

		pkgs[i], err = parsePackage(ctx, path)
		if err != nil {
			return nil, err
		}

		pkgs[i].IsLocal = true
	}

	return pkgs, nil
}

var packageCache = make(map[string]*Package)

func parsePackage(ctx context.Context, rootPath string) (*Package, error) {
	if pkg, ok := packageCache[rootPath]; ok {
		return pkg, nil
	}

	fset := token.NewFileSet()
	files := make(map[string]*ast.File)
	fileNames := make([]string, 0)
	testFileNames := make([]string, 0)

	buildCtx := build.Default

	err := filepath.Walk(rootPath, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		relPath, err := filepath.Rel(rootPath, path)
		if err != nil {
			return err
		}

		if info.IsDir() ||
			!strings.HasSuffix(info.Name(), ".go") ||
			strings.Contains(relPath, "/") {
			return nil
		}

		contents, err := ioutil.ReadFile(path)
		if err != nil {
			return err
		}

		dir, fileName := filepath.Split(path)
		ok, err := buildCtx.MatchFile(dir, fileName)
		if err != nil {
			return err
		}

		if !ok {
			return nil
		}

		file, err := parser.ParseFile(fset, path, contents, parser.ImportsOnly)
		if err != nil {
			return err
		}

		files[fileName] = file

		if strings.HasSuffix(fileName, "_test.go") {
			testFileNames = append(testFileNames, fileName)
		} else {
			fileNames = append(fileNames, fileName)
		}

		return nil
	})
	if err != nil {
		return nil, err
	}

	pkgName := ""
	imports := make(map[string]*struct{})
	testImports := make(map[string]*struct{})
	for fileName, file := range files {
		filePkgName := file.Name.String()
		isTestFile := strings.HasSuffix(fileName, "_test.go")
		if isTestFile {
			filePkgName = strings.TrimSuffix(filePkgName, "_test")
		}

		if pkgName == "" {
			pkgName = filePkgName
		} else if pkgName != filePkgName {
			return nil, fmt.Errorf("conflicting package names in %v: %v and %v", rootPath, pkgName, filePkgName)
		}

		for _, i := range file.Imports {
			importPath := strings.Trim(i.Path.Value, "\"")
			if isTestFile {
				testImports[importPath] = nil
			} else {
				imports[importPath] = nil
			}
		}
	}

	resolveImports := func(imports map[string]*struct{}) ([]string, error) {
		canonicalImports := make([]string, 0)
		for i := range imports {
			// Resolve relative imports
			if build.IsLocalImport(i) {
				pkg, err := buildCtx.Import(i, rootPath, build.FindOnly)
				if err != nil {
					return nil, err
				}

				i = pkg.ImportPath
			}

			if importIsStdlib(i) {
				continue
			}

			canonicalImports = append(canonicalImports, i)
		}

		return canonicalImports, nil
	}

	canonicalImports, err := resolveImports(imports)
	if err != nil {
		return nil, err
	}

	canonicalTestImports, err := resolveImports(testImports)
	if err != nil {
		return nil, err
	}

	pkg := &Package{
		Name:            pkgName,
		Path:            rootPath,
		SourceFiles:     fileNames,
		TestSourceFiles: testFileNames,
		Imports:         canonicalImports,
		TestImports:     canonicalTestImports,
		IsMain:          pkgName == "main",
	}

	packageCache[rootPath] = pkg

	return pkg, nil
}

func importIsStdlib(path string) bool {
	parts := strings.Split(path, "/")
	for _, s := range stdlib {
		if parts[0] == s {
			return true
		}
	}
	return false
}

// We need to filter out stdlib imports, but importing and checking
// the Goroot flag is expensive. A list of stdlib roots is 'good
// enough'
var stdlib = []string{
	"archive",
	"bufio",
	"builtin",
	"bytes",
	"C",
	"cmd",
	"compress",
	"container",
	"context",
	"crypto",
	"database",
	"debug",
	"encoding",
	"errors",
	"expvar",
	"flag",
	"fmt",
	"go",
	"hash",
	"html",
	"image",
	"index",
	"io",
	"log",
	"math",
	"mime",
	"net",
	"os",
	"path",
	"plugin",
	"reflect",
	"regexp",
	"runtime",
	"sort",
	"strconv",
	"strings",
	"sync",
	"syscall",
	"testing",
	"text",
	"time",
	"unicode",
	"unsafe",
}
