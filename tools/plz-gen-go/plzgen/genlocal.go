package plzgen

import (
	"path/filepath"
	"text/template"
)

func writeLocalBuildFiles(pkgs []*Package, resolver *Resolver) error {
	for _, pkg := range pkgs {
		err := writeLocalBuildFile(pkg, resolver)
		if err != nil {
			return err
		}
	}
	return nil
}

type localTmplData struct {
	Name            string
	ImportPath      string
	IsMain          bool
	BinaryName      string
	SourceFiles     []string
	Deps            map[string]*struct{}
	TestSourceFiles []string
	TestDeps        map[string]*struct{}
}

var localBuildTmpl = template.Must(template.New("").Parse(`
subrepo = subrepo_name()

subrepo = f"///{subrepo}" if subrepo else ""

{{ if .SourceFiles }}
{{- if .IsMain }}go_binary{{ else }}go_library{{ end }}(
{{- if .IsMain }}
    name = "{{ .BinaryName }}",
{{- else }}
    name = "{{ .Name }}",
{{- end }}
    srcs = [
{{- range .SourceFiles }}
        "{{ . }}",
{{- end }}
    ],
{{- if not .IsMain }}
    visibility = ["//..."],
{{- end }}
    deps = [
{{- range $k, $v := .Deps }}
        "{{ $k }}",
{{- end }}
    ],
{{- if not .IsMain }}
    import_path = "{{ .ImportPath }}",
{{- end }}
)
{{ end }}
{{- if .TestSourceFiles }}
go_test(
    name = "test",
    srcs = [
{{- range .TestSourceFiles }}
        "{{ . }}",
{{- end }}
    ],
    visibility = ["//..."],
    deps = [
{{- range $k, $v := .TestDeps }}
        "{{ $k }}",
{{- end }}
    ],
)
{{end}}
`))

func writeLocalBuildFile(pkg *Package, resolver *Resolver) error {
	data := localTmplData{
		Name:            pkg.Name,
		ImportPath:      filepath.Join(resolver.LocalModule, pkg.Path),
		IsMain:          pkg.IsMain,
		SourceFiles:     pkg.SourceFiles,
		TestSourceFiles: pkg.TestSourceFiles,
		Deps:            make(map[string]*struct{}),
		TestDeps:        make(map[string]*struct{}),
	}

	if pkg.IsMain {
		_, data.BinaryName = filepath.Split(pkg.Path)
	}

	for _, imp := range pkg.Imports {
		dep, err := resolver.Resolve(imp)
		if err != nil {
			return err
		}
		data.Deps[dep] = nil
	}

	for _, imp := range pkg.TestImports {
		dep, err := resolver.Resolve(imp)
		if err != nil {
			return err
		}
		data.TestDeps[dep] = nil
	}

	generatedPath := filepath.Join(pkg.Path, "BUILD")
	err := templateBuildFile(generatedPath, localBuildTmpl, data)
	if err != nil {
		return err
	}

	return nil
}
