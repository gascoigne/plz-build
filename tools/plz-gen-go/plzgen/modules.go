package plzgen

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"path/filepath"
	"strings"

	"golang.org/x/mod/modfile"
)

func parseModFile() (*modfile.File, error) {
	buf, err := ioutil.ReadFile("go.mod")
	if err != nil {
		return nil, err
	}

	return modfile.Parse("go.mod", buf, nil)
}

type Module struct {
	Path    string
	Version string
	Replace *Module
	Dir     string

	Deps             map[string]*Module   `json:"-"` // key: path
	RequiredPackages map[string]*struct{} `json:"-"`
}

func moduleBuildList(ctx context.Context) (map[string]*Module, error) {
	// must first "go mod download" to ensure we have a 'Dir' attribute for all modules
	_, err := Go.Run(ctx, "mod", "download")
	if err != nil {
		return nil, err
	}

	output, err := Go.RunFull(ctx, "list", "-mod=readonly", "-m", "-json", "all")
	if err != nil {
		return nil, err
	}

	decoder := json.NewDecoder(bytes.NewBufferString(output))

	buildlist := make(map[string]*Module)
	for decoder.More() {
		var module Module
		err = decoder.Decode(&module)
		if err != nil {
			return nil, err
		}

		module.Deps = make(map[string]*Module)
		module.RequiredPackages = make(map[string]*struct{})

		if module.Replace != nil {
			module.Replace.Deps = make(map[string]*Module)
			module.Replace.RequiredPackages = make(map[string]*struct{})
		}

		buildlist[module.Path] = &module
	}

	return buildlist, nil
}

func buildModuleDeps(ctx context.Context, localPackages []*Package, resolver *Resolver) error {
	visitedPackages := make(map[string]*Package)
	for _, pkg := range localPackages {
		err := buildModuleDepsInner(ctx, resolver, pkg.Path, visitedPackages)
		if err != nil {
			return err
		}
	}
	return nil
}

func buildModuleDepsInner(ctx context.Context, resolver *Resolver, path string, visitedPackages map[string]*Package) error {
	if _, ok := visitedPackages[path]; ok {
		return nil
	}

	var err error
	visitedPackages[path], err = parsePackage(ctx, path)
	if err != nil {
		return err
	}

	pkg := visitedPackages[path]

	var module *Module
	if !pkg.IsLocal {
		module, err = resolver.ResolveModuleByDir(pkg.Path)
		if err != nil {
			return err
		}

		if module.Replace != nil {
			module = module.Replace
		}
	}

	for _, importedPath := range pkg.Imports {
		if resolver.PackageIsInLocalModule(importedPath) {
			continue
		}

		importedModule, err := resolver.ResolveModule(importedPath)
		if err != nil {
			return err
		}

		realImportedModule := importedModule
		if importedModule.Replace != nil {
			realImportedModule = importedModule.Replace
		}

		// For third party packages, record any dependencies on other
		// third party modules
		if !pkg.IsLocal && !strings.HasPrefix(importedPath, module.Path) {
			module.Deps[importedModule.Path] = nil
		}

		// compute path to package within module dir
		if !strings.HasPrefix(importedPath, importedModule.Path) {
			return fmt.Errorf("expected import path %v to be rooted under module %v", importedPath, importedModule.Path)
		}
		relativePath := strings.TrimPrefix(importedPath, importedModule.Path)
		relativePath = strings.TrimLeft(relativePath, "/")

		importedPackagePath := filepath.Join(realImportedModule.Dir, relativePath)

		// update the module to record that the package is required
		realImportedModule.RequiredPackages[relativePath] = nil

		// recurse into the imported package
		err = buildModuleDepsInner(ctx, resolver, importedPackagePath, visitedPackages)
		if err != nil {
			return err
		}
	}

	return nil
}
