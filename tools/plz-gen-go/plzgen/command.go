package plzgen

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"os/exec"
	"strings"

	"github.com/sirupsen/logrus"
)

// Error is an error which is returned when the `zfs` or `zpool` shell
// commands return with a non-zero exit code.
type Error struct {
	Err    error
	Debug  string
	Stderr string
}

// Error returns the string representation of an Error.
func (e Error) Error() string {
	return fmt.Sprintf("%s: %q => %s", e.Err, e.Debug, e.Stderr)
}

type Command struct {
	Command string
	Stdin   io.Reader
	Stdout  io.Writer
}

func (c *Command) RunFull(ctx context.Context, arg ...string) (string, error) {
	cmd := exec.CommandContext(ctx, c.Command, arg...)

	var stdout, stderr bytes.Buffer

	if c.Stdout == nil {
		cmd.Stdout = &stdout
	} else {
		cmd.Stdout = c.Stdout
	}

	if c.Stdin != nil {
		cmd.Stdin = c.Stdin
	}
	cmd.Stderr = &stderr

	joinedArgs := strings.Join(cmd.Args, " ")

	logrus.Debugf("# %v", joinedArgs)
	err := cmd.Run()
	if err != nil {
		logrus.Errorf("failed (err): %v", err)
		logrus.Errorf("failed (stdout):\n%v", stdout.String())
		logrus.Errorf("failed (stderr):\n%v", stderr.String())
		return "", &Error{
			Err:    err,
			Debug:  cmd.Path + " " + strings.Join(cmd.Args[1:], " "),
			Stderr: stderr.String(),
		}
	}

	// assume if you passed in something for stdout, that you know what to do with it
	if c.Stdout != nil {
		return "", nil
	}

	logrus.Debugf("output:\n:%v", stdout.String())

	return stdout.String(), nil
}

func (c *Command) RunLines(ctx context.Context, arg ...string) ([]string, error) {
	out, err := c.RunFull(ctx, arg...)
	if err != nil {
		return nil, err
	}

	lines := strings.Split(out, "\n")

	// last line is always blank
	lines = lines[0 : len(lines)-1]
	return lines, nil
}

func (c *Command) Run(ctx context.Context, arg ...string) ([][]string, error) {
	lines, err := c.RunLines(ctx, arg...)
	if err != nil {
		return nil, err
	}

	output := make([][]string, len(lines))
	for i, l := range lines {
		output[i] = strings.FieldsFunc(l, func(r rune) bool {
			return r == '\t'
		})
	}

	return output, nil
}
