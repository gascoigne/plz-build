package cmd

import (
	"github.com/spf13/cobra"

	"gitlab.com/gascoigne/plz-build/tools/plz-gen-go/plzgen"
)

// localCmd represents the local command
var localCmd = &cobra.Command{
	Use:   "local",
	Short: "Generate build definitions for local packages",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		plzgen.UpdateLocal()
	},
}

func init() {
	rootCmd.AddCommand(localCmd)
}
