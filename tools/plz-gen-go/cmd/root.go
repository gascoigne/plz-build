package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"

	"gitlab.com/gascoigne/plz-build/tools/plz-gen-go/plzgen"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "plz-gen-go",
	Short: "",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		plzgen.UpdateLocal()
	},
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
