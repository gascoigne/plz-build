package ocitool

import (
	containerv1 "github.com/google/go-containerregistry/pkg/v1"
	"github.com/google/go-containerregistry/pkg/v1/mutate"
)

type ImageConfig struct {
	Tag        string
	Entrypoint []string
}

func applyConfig(image containerv1.Image, newConfig *ImageConfig) (containerv1.Image, error) {
	config, err := image.ConfigFile()
	if err != nil {
		return nil, err
	}

	if newConfig.Entrypoint != nil {
		config.Config.Entrypoint = newConfig.Entrypoint
	}

	return mutate.ConfigFile(image, config)
}
