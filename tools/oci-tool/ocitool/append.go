package ocitool

import (
	"context"

	containerv1 "github.com/google/go-containerregistry/pkg/v1"
	"github.com/google/go-containerregistry/pkg/v1/mutate"
	"github.com/google/go-containerregistry/pkg/v1/tarball"
)

func AppendLayers(ctx context.Context, base, dest string, config *ImageConfig, layerPaths []string) error {
	image, err := loadImage(base)
	if err != nil {
		return err
	}

	layers := make([]containerv1.Layer, len(layerPaths))
	for i, p := range layerPaths {
		layers[i], err = tarball.LayerFromFile(p)
		if err != nil {
			return err
		}
	}

	image, err = mutate.AppendLayers(image, layers...)
	if err != nil {
		return err
	}

	err = saveImage(dest, config, image)
	if err != nil {
		return err
	}

	return nil
}
