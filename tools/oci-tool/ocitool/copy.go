package ocitool

import (
	"context"
)

func CopyImage(ctx context.Context, source, dest string, config *ImageConfig) error {
	image, err := loadImage(source)
	if err != nil {
		return err
	}

	err = saveImage(dest, config, image)
	if err != nil {
		return err
	}

	return nil
}
