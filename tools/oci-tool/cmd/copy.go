package cmd

import (
	"context"
	"os"

	"github.com/spf13/cobra"

	"gitlab.com/gascoigne/plz-build/tools/oci-tool/ocitool"
)

// copyCmd represents the copy command
var copyCmd = &cobra.Command{
	Use:       "copy",
	Short:     "Copy an image between representations",
	Long:      ``,
	Args:      cobra.MinimumNArgs(2),
	ValidArgs: []string{"image", "destination"},
	Run: func(cmd *cobra.Command, args []string) {
		source := args[0]
		dest := args[1]

		config, err := parseCommonFlags(cmd)
		if err != nil {
			cmd.Println(err)
			os.Exit(1)
		}

		ctx := context.Background()
		err = ocitool.CopyImage(ctx, source, dest, config)
		if err != nil {
			cmd.Println(err)
			os.Exit(1)
		}
	},
}

func init() {
	rootCmd.AddCommand(copyCmd)

	addCommonFlags(copyCmd)
}
