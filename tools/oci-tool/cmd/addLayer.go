package cmd

import (
	"context"
	"os"

	"github.com/spf13/cobra"

	"gitlab.com/gascoigne/plz-build/tools/oci-tool/ocitool"
)

// addLayerCmd represents the addLayer command
var addLayerCmd = &cobra.Command{
	Use:       "add-layers",
	Short:     "Append a layer to a given base image",
	Long:      ``,
	Args:      cobra.MaximumNArgs(3),
	ValidArgs: []string{"image", "dest", "layer"},
	Run: func(cmd *cobra.Command, args []string) {
		base := args[0]
		dest := args[1]
		layers := args[2:]

		config, err := parseCommonFlags(cmd)
		if err != nil {
			cmd.Println(err)
			os.Exit(1)
		}

		ctx := context.Background()
		err = ocitool.AppendLayers(ctx, base, dest, config, layers)
		if err != nil {
			cmd.Println(err)
			os.Exit(1)
		}
	},
}

func init() {
	rootCmd.AddCommand(addLayerCmd)

	addCommonFlags(addLayerCmd)
}
