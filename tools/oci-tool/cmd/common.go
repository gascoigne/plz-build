package cmd

import (
	"github.com/spf13/cobra"

	"gitlab.com/gascoigne/plz-build/tools/oci-tool/ocitool"
)

func addCommonFlags(cmd *cobra.Command) {
	cmd.Flags().String("tag", "", "override the name of the resulting image")
	cmd.Flags().StringArray("entrypoint", nil, "override the entry point of the resulting image")
}

func parseCommonFlags(cmd *cobra.Command) (*ocitool.ImageConfig, error) {
	tag, err := cmd.Flags().GetString("tag")
	if err != nil {
		return nil, err
	}

	entrypoint, err := cmd.Flags().GetStringArray("entrypoint")
	if err != nil {
		return nil, err
	}

	return &ocitool.ImageConfig{
		Tag:        tag,
		Entrypoint: entrypoint,
	}, nil
}
